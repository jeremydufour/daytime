# *****************************************************************************
# Daytime   An application to help you track your daily activity
# Copyright (C) 2021  Jeremy DUFOUR
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For more information please see <https://www.dufour.work>.
# *****************************************************************************


def hourConvert(hour):
    # Convert hour in format "00:00" to a float scaled from 0 to 24
    result = 0
    if(len(hour) == 4):
        h = int(hour[0])
        m = round(int(hour[2:4])/60, 2)
    else:
        h = int(hour[0:2])
        m = round(int(hour[3:5])/60, 2)
    result = h + m
    return result
#END hourConvert

def dayConvert(index):
    if index == 1:
        return "Monday"
    elif index == 2:
        return "Tuesday"
    elif index == 3:
        return "Wednesday"
    elif index == 4:
        return "Thursday"
    elif index == 5:
        return "Friday"
    elif index == 6:
        return "Saturday"
    elif index == 7:
        return "Sunday"
    else:
        print("[LibUtil-dayConvert()] Day index is invalid")
        return -1
#END dayConvert

def monthConvert(index):
    if index == 1:
        return "January"
    elif index == 2:
        return "February"
    elif index == 3:
        return "March"
    elif index == 4:
        return "April"
    elif index == 5:
        return "May"
    elif index == 6:
        return "June"
    elif index == 7:
        return "July"
    elif index == 8:
        return "August"
    elif index == 9:
        return "September"
    elif index == 10:
        return "October"
    elif index == 11:
        return "November"
    elif index == 12:
        return "December"
    else:
        print("[LibUtil-monthConvert()] Month index is invalid")
        return -1
#END monthConvert

def parseRgbCode(colorCode):
    #Parsing Gdk rgb code and return red/blue/green values

    red = -1
    blue = -1
    green = -1
    temp = ""
    for char in colorCode[4:]:
        if char == " ":
            continue
        if char == "," or char == ")":
            if red == -1:
                red = int(temp)
                temp = ""
                continue
            if blue == -1:
                blue = int(temp)
                temp = ""
                continue
            if green == -1:
                green = int(temp)
                temp = ""
                break
        else:
            temp += char

    return [red, blue, green]
#END parseRgbCode

def getTextColor(bgColor):
    rgb = parseRgbCode(bgColor)
    red = rgb[0]
    blue = rgb[1]
    green = rgb[2]

    # Calculating perceived brightness
    # Inspired from W3C recommendations: https://www.w3.org/TR/AERT/#color-contrast
    brightness = red*0.299 + blue*0.587 + green*0.114

    if brightness >= 128:
        #black
        textColor = "rgb(0,0,0)"
    else:
        #white
        textColor = "rgb(255,255,255)"

    return textColor
#END getTextColor
