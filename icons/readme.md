# Daytime icon guide

The icon was made using [GNOME icon template and palette](https://gitlab.gnome.org/Teams/Design/HIG-app-icons/-/tree/master) on Inkscape.

#### Exporting Daytime icon on Inkscape:
When you're done editing, duplicate the objects forming the icon and group them into a single svg.

In the exporting menu it is recommended to:
- use a size of 130 x 152 px
- export only the selection

Happy editing!
