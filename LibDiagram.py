# *****************************************************************************
# Daytime   An application to help you track your daily activity
# Copyright (C) 2021  Jeremy DUFOUR
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For more information please see <https://www.dufour.work>.
# *****************************************************************************


## External modules ------------------------------------
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk

from matplotlib.figure import Figure as pltFig
import matplotlib.pyplot as plt
import matplotlib.axes as pltAxes
import matplotlib.dates as pltDates

from matplotlib.backends.backend_gtk3agg import FigureCanvas


## Local modules ---------------------------------------
import LibSettings
import LibUtil

## Global variables ------------------------------------
EMPTY_WEEK = {"1": [], "2": [], "3":[], "4":[], "5":[], "6":[], "7":[]}



def formatData(dataList):
    rangeTasks = []
    taskIndex = {}
    taskCount = 0

    for day in range(1,8):
        registered = {}
        for task in dataList[str(day)]:
            if task == []:
                break
            else:
                yData = [0,0,0,0,0,0,0]
                bottom = [0,0,0,0,0,0,0]
                start = LibUtil.hourConvert(task[1])
                stop = LibUtil.hourConvert(task[2])

                # Affect data here
                yData[day-1] = stop - start
                bottom[day-1] = start
                try:
                    registered[task[0]]+=1

                    amount = registered[task[0]]
                    if len(taskIndex[task[0]]) < amount:
                        taskIndex[task[0]].append(taskCount)
                        rangeTasks.append([task[0], yData, bottom])
                        taskCount+=1
                    else:
                        # Handle existing task from a previous day
                        globalCount = taskIndex[task[0]][amount-1]
                        # Affect data here
                        rangeTasks[globalCount][1][day-1] = stop - start   # yData
                        rangeTasks[globalCount][2][day-1] = start   # bottom

                except KeyError:
                    registered[task[0]] = 1
                    try:
                        if 1 < taskCount:
                            # Handle existing task from a previous day
                            globalCount = taskIndex[task[0]][0]
                            # Affect data here
                            rangeTasks[globalCount][1][day-1] = stop - start  # bottom
                            rangeTasks[globalCount][2][day-1] = start  # yData
                        else:
                            taskIndex[task[0]].append(taskCount)
                            rangeTasks.append([task[0], yData, bottom])
                            taskCount+=1
                    except KeyError:
                        taskIndex[task[0]] = [taskCount]
                        rangeTasks.append([task[0], yData, bottom])
                        taskCount+=1
    return rangeTasks
#END formatData


class barDiagram(Gtk.Box):
    def __init__(self, taskList):
        super().__init__(self, homogeneous=False, spacing=8, vexpand=True, hexpand=False)

        label = Gtk.Label(label='Plot data')

        # Diagram area
        self.fig = pltFig(figsize=(100, 100), facecolor=(0.98,0.98,0.98,0.0), frameon=False)
        self.plot = self.fig.add_subplot(1, 1, 1)

        # Add figure
        canvas = FigureCanvas(self.fig)
        self.pack_start(canvas, True, True, 1)

        self.__draw__(taskList)
    #END __init__

    def __draw__(self, taskList):
        labels = ['Mo', 'Tu', 'We', 'Th', 'Fr','Sa', 'Su']
        barWidth = 0.93

        if taskList == EMPTY_WEEK:
            barPlot = self.plot.bar(labels, [0,0,0,0,0,0,0], barWidth, yerr=None, label="No tasks")
            self.fig.canvas.draw_idle()
        else:
            # Format x and y data with the tasks in taskList
            data = formatData(taskList)

            for i in range(0,len(data)):
                colorData = LibSettings.getColor(data[i][0])
                plotColor = Gdk.RGBA()
                plotColor.parse(colorData)

                self.plot.bar(labels, data[i][1], barWidth, yerr=None, bottom=data[i][2], label=data[i][0],
                         color=(plotColor.red, plotColor.green, plotColor.blue))
                self.fig.canvas.draw_idle()

        self.plot.set_frame_on(True)
        self.plot.set_xmargin(0.01)
        self.plot.yaxis.set_visible(True)
        self.plot.set_ylim(0.0,24.0)
        self.plot.invert_yaxis()
        self.plot.tick_params(top=True, bottom=False, labeltop=True, labelbottom=False)

        self.plot.set_yticks([0,3,6,9,12,15,18,21,23.98])
        self.plot.set_yticklabels(["0:00","3:00","6:00","9:00","12:00","15:00","18:00","21:00","23:59"])

        self.plot.grid(which='both', axis='y')

        self.show_all()
    #END __draw__

    def __refresh__(self, taskList):
        subPlot = self.fig.get_children()[1]
        subPlot.clear()
        self.__draw__(taskList)
    #END __refresh__
#END barDiagram


class progressDiagram(Gtk.Box):
    def __init__(self, progressData, showEmpty, showHours):
        super().__init__(self, homogeneous=False, spacing=8, vexpand=True, hexpand=False)

        # Diagram area
        self.fig = pltFig(figsize=(7, 5), facecolor=(0.98,0.98,0.98,0.0))
        self.plot = self.fig.add_subplot(1, 1, 1, anchor='E')
        self.plot.patch.set_alpha(0.0)
        self.plot.set_frame_on(False)

        # Add figure
        canvas = FigureCanvas(self.fig)
        self.pack_start(canvas, True, True, 1)

        self.__draw__(progressData, showEmpty, showHours)
    #END __init__

    def __draw__(self, progressData, showEmpty, showHours):
        #Add pie chart
        names = progressData[0]
        colorList = []
        legendData = []
        plotData = []

        if showEmpty == True:
            plotData = progressData[1]
            names.append("Untracked")
        else:
            plotData = progressData[2]
        dataLen = len(plotData)

        for i in range(0, dataLen):
            if (showEmpty == True) and (i == (dataLen-1)):
                colorList.append([0.95, 0.95, 0.95])
            else:
                colorData = LibSettings.getColor(names[i])
                plotColor = Gdk.RGBA()
                plotColor.parse(colorData)
                colorList.append([plotColor.red, plotColor.green, plotColor.blue])
            if showHours == True:
                time = progressData[1][i] * 167.86   # Considering 167.86 hours in a week
                value = "{0:.1f}".format(time)
                legendData.append(names[i] + "  " + str(value) + "h")
            else:
                value = "{0:.1%}".format(plotData[i])
                legendData.append(names[i] + "  " + str(value))

        graphs = self.plot.pie(plotData, colors=colorList,
            normalize=True,
            #autopct='%1.1f%%',
            startangle=90,
            textprops={'size': 'smaller'}
            )
        self.fig.canvas.draw_idle()

        if showEmpty == True:
            graphs[0][dataLen-1].set_hatch('/')
            graphs[0][dataLen-1].set_edgecolor([0.85, 0.85, 0.85])
        elif dataLen == 0:
            self.plot.text(0.15, 0.5, 'Nothing here!\n\nGet progress by entering your first task',
                    horizontalalignment='center',
                    verticalalignment='center',
                    fontsize=15, color='darkgray',
                    wrap=True,
                    transform=self.plot.transAxes)

        self.plot.legend(legendData,
                        title="Tasks",
                        loc="upper left",
                        bbox_to_anchor=(-0.9, 0, 1, 1)
                        )
        self.plot.patch.set_alpha(0.0)
        self.plot.set_frame_on(False)

        self.show_all()

        names = None
    #END __draw__

    def __refresh__(self, progressData, showEmpty, showHours):
        subPlot = self.fig.get_children()[1]
        subPlot.clear()
        self.__draw__(progressData, showEmpty, showHours)
    #END __refresh__

#END progressDiagram
