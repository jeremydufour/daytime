# *****************************************************************************
# Daytime   An application to help you track your daily activity
# Copyright (C) 2021  Jeremy DUFOUR
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For more information please see <https://www.dufour.work>.
# *****************************************************************************

## External modules ------------------------------------
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk
import random as Rdm
import json
from pathlib import Path
import datetime as DT


## Local modules ---------------------------------------
import LibSettings
import LibDiagram
import LibUtil


## Global variables ------------------------------------
DATA_PATH = LibSettings.DATA_PATH

EMPTY_WEEK = {"1": [], "2": [], "3":[], "4":[], "5":[], "6":[], "7":[]}

taskData = None
dayNb = 0
weekNb = 0
yearNb = 0


def dayTaskSort(tasks):
    tasks.sort(key=lambda val : LibUtil.hourConvert(val[1]) if val!=[] else val)
    return 0
#END dayTaskSort

def globalTaskSort(data):
    keyList = list(data.keys())
    for i in keyList:
        keys = list(data[i].keys())
        for j in keys:
            dayTaskSort(data[i][j])

    return 0
#END globalTaskSort


def loadData(year,week,day):
    global taskData
    global weekNb
    global yearNb
    global dayNb

    #DATA_PATH defined in LibSettings
    fileName = DATA_PATH + str(year) + "-tasks.json"
    # handling FileNotFoundError from open()
    try:
        dataFile = open(fileName, "r+")

        taskData = json.load(dataFile) # Reads the file and decodes
    except FileNotFoundError:
        print("[LibTask-loadData()] File doesn't exist, we create it then...")
        Path(fileName).touch()
        dataFile = open(fileName, "r+")
        w = "w_" + str(week)
        taskData = {w:EMPTY_WEEK}

    dataFile.close()
    globalTaskSort(taskData)

    yearNb = year
    weekNb = week
    dayNb = day
#END loadData

def saveData():
    global taskData
    global yearNb

    fileName = DATA_PATH + str(yearNb) + "-tasks.json"
    print("[Daytime] Saving task data at " + fileName)
    # handling FileNotFoundError from open()
    try:
        dataFile = open(fileName, "w+")
    except FileNotFoundError:
        print("[LibTask-saveData()] File doesn't exist, we create it then...")
        Path(fileName).touch()
        dataFile = open(fileName, "w+")

    # Append json content to file
    json.dump(taskData, dataFile, indent=2, sort_keys=True)
    dataFile.close()
#END saveData

def getLastTime():
    global taskData
    global weekNb
    global dayNb

    try:
        lastTime = taskData["w_"+str(weekNb)][str(dayNb)][-1][2]
        return lastTime
    except KeyError:
        #print("[LibTask-getLastTime()] Week is empty")
        return "00:00"
    except IndexError:
        #print("[LibTask-getLastTime()] Day is empty")
        return "00:00"

    return "00:00"
#END getLastTime

def prevWeek():
    global weekNb
    global yearNb
    global taskData

    week = weekNb - 1
    try:
        taskData["w_"+str(week)]
        weekNb = week
    except KeyError:
        if (week > 0):
            weekNb = week
            taskData["w_"+str(week)] = EMPTY_WEEK
        else:
            isoDate = DT.date((int(yearNb)-1),12,28).isocalendar()
            saveData()
            loadData(isoDate[0], isoDate[1], 7)
#END prevWeek


def nextWeek():
    global weekNb
    global yearNb
    global taskData

    week = weekNb + 1
    try:
        taskData["w_"+str(week)]
        weekNb = week
    except KeyError:
        try:
            isoDate = DT.date.fromisocalendar(int(yearNb),week,1)

            taskData["w_"+str(week)] = EMPTY_WEEK
            weekNb = week
        except ValueError:
            saveData()
            loadData((int(yearNb)+1), 1, 1)
#END nextWeek

def randomColor():
    colorCode = "rgb("
    red = Rdm.randrange(0, 256, 1)
    blue = Rdm.randrange(0, 256, 1)
    green = Rdm.randrange(0, 256, 1)

    return (colorCode + str(red) + "," + str(blue) + "," + str(green) + ")")
#END randomColor

class simpleTask(Gtk.Box):
    def __init__(self, name, tStart, tStop):
        Gtk.Box.__init__(self)
        self.set_orientation(Gtk.Orientation.HORIZONTAL)
        self.set_size_request(-1,40)
        # Set background color
        bgColor = Gdk.RGBA(Rdm.random(),Rdm.random(),Rdm.random(),1)
        taskColor = LibSettings.getColor(name)
        if taskColor != -1:
            bgColor.parse(taskColor)
        else:
            LibSettings.setColor(name, (bgColor.to_string()))
        self.override_background_color(Gtk.StateFlags.NORMAL, bgColor)

        # Set label and font color (default black)
        fontColor = Gdk.RGBA(0,0,0,1)
        fontColor.parse(LibUtil.getTextColor(bgColor.to_string()))
        nameLabel = Gtk.Label(label=name)
        nameLabel.override_color(Gtk.StateFlags.NORMAL, fontColor)
        self.pack_start(nameLabel,False,False,20)

        stopLabel = Gtk.Label(label=tStop)
        stopLabel.override_color(Gtk.StateFlags.NORMAL, fontColor)
        self.pack_end(stopLabel,False,False,20)
        startLabel = Gtk.Label(label=tStart)
        startLabel.override_color(Gtk.StateFlags.NORMAL, fontColor)
        self.pack_end(startLabel,False,False,10)
        self.show_all()
#END simpleTask

class activityDay(Gtk.ListBox):
    def __init__(self, day=dayNb):
        global taskData
        global weekNb
        global dayNb

        Gtk.ListBox.__init__(self)
        self.set_selection_mode(Gtk.SelectionMode.SINGLE)
        self.set_vexpand(False)
        self.show_all()

        self.pointed = False

        try:
            dayTasks = taskData["w_"+str(weekNb)][str(day)]
        except KeyError:
            if day<1 and day>7:
                print("[LibTask-activityDay()] Day format error")
                return None
            print("[LibTask-activityDay()] No tasks available for this day")
            w = "w_" + str(weekNb)
            taskData[w] = EMPTY_WEEK
            dayTasks = taskData["w_"+str(weekNb)][str(day)]

        nbActivity = len(dayTasks)
        for i in range(0,nbActivity):
            aName = dayTasks[i][0]
            task = simpleTask(aName,dayTasks[i][1],dayTasks[i][2])
            self.insert(task,-1)
            task.show()
        dayNb = day
    #END __init__

    def pointed_in(self, widget, event):
        self.pointed = True
    #END pointed_in

    def pointed_out(self, widget, event):
        self.pointed = False
    #END pointed_in
#END activityDay

def addTask(name, start, stop):
    global taskData
    global weekNb
    global dayNb

    try:
        dayTasks = taskData["w_"+str(weekNb)][str(dayNb)]

        taskData["w_"+str(weekNb)][str(dayNb)].append([name, start, stop])
        dayTaskSort(taskData["w_"+str(weekNb)][str(dayNb)])
    except KeyError:
        taskData["w_"+str(weekNb)][str(dayNb)] = [[name, start, stop]]

    # Color
    if LibSettings.getColor(name) == -1:
        LibSettings.setColor(name,randomColor())

    return 0
#END addTask

def deleteTask(index):
    global taskData
    global weekNb
    global dayNb

    dayTasks = taskData["w_"+str(weekNb)][str(dayNb)]
    #delete task
    taskData["w_"+str(weekNb)][str(dayNb)].pop(index)
    return 0
#END deleteTask


#-------------- Task progress --------------------------
def getProgress():
    global taskData
    global weekNb

    weekData = taskData["w_"+str(weekNb)]
    tasks = []
    totalProgress = []  #Progress of the entire week
    inputProgress = []  #Progress of the time tracked by the user
    totalTime = 167.86  #total hours in a week (a day is 23.98 hours in Daytime)
    inputTime = 0

    for i in range(1,8):
        dayTasks = weekData[str(i)]
        nbActivity = len(dayTasks)

        for j in range(0,nbActivity):
            task = dayTasks[j]
            taskName = task[0]
            start = LibUtil.hourConvert(task[1])
            stop = LibUtil.hourConvert(task[2])
            progress = (stop - start)
            inputTime += progress
            if taskName in tasks:
                taskIndex = tasks.index(taskName)
                totalProgress[taskIndex] += progress / totalTime
                inputProgress[taskIndex] += progress
            else:
                tasks.append(taskName)
                totalProgress.append(progress / totalTime)
                inputProgress.append(progress)

    totalProgress.append((totalTime - inputTime) / totalTime)

    # Compute input progress in percentage
    for p in range(0,len(inputProgress)):
        inputProgress[p] = (inputProgress[p] / inputTime)

    return [tasks, totalProgress, inputProgress]
#END getTotalProgress
