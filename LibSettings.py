# *****************************************************************************
# Daytime   An application to help you track your daily activity
# Copyright (C) 2021  Jeremy DUFOUR
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For more information please see <https://www.dufour.work>.
# *****************************************************************************


import json
from pathlib import Path

## Global variables
DATA_PATH = "~/.daytime/"

settings = None


def loadSettings():
    global settings

    fileName = DATA_PATH + "settings.json"
    # handling FileNotFoundError from open()
    try:
        dataFile = open(fileName, "r+")

        settings = json.load(dataFile) # Reads the file and decodes
    except FileNotFoundError:
        print("[LibSettings-loadSettings()]: Parameter File doesn't exist, we create it then...")
        Path(fileName).touch()
        dataFile = open(fileName, "r+")
        settings = {"displayEmptyTime":False,"hourProgress":False,"taskParam":{}}

    dataFile.close()
#END loadSettings

def saveSettings():
    global settings

    fileName = DATA_PATH + "settings.json"
    print("[Daytime] Saving settings at " + fileName)

    # handling FileNotFoundError from open()
    try:
        dataFile = open(fileName, "w+")
    except FileNotFoundError:
        print("[LibSettings-loadSettings()]: Paramter file doesn't exist, we create it then...")
        Path(fileName).touch()
        dataFile = open(fileName, "w+")
    # Erase the file to avoid json extra content
    #dataFile.truncate(0)
    # Append json content to file
    json.dump(settings, dataFile, indent=2, sort_keys=True)
    dataFile.close()
#END saveSettings

def deleteSetting(setting, name=None):
    if setting == "taskParam":
        settings["taskParam"].pop(name)

    elif setting == "displayEmptyTime":
        settings["displayEmptyTime"] = False

    elif setting == "hourProgress":
        settings["hourProgress"] = False

    elif setting == "all":
        settings["taskParam"] = {}
        settings["displayEmptyTime"] = False
        settings["hourProgress"] = False

    return 0
#END deleteSetting

def getColor(task):
    global settings

    try:
        taskParam = settings["taskParam"]
        try:
            color = taskParam[task]
            return color
        except KeyError:
            print("[LibSettings-getColor()]: \"",task,"\" Task not found in settings")
            return -1
    except KeyError:
        print("[LibSettings-getColor()]: No \"taskParam\" parameter found")
        return -1
#END getColor

def setColor(task, color):
    global settings

    settings["taskParam"][task] = color
#END setColor


#################################################################
#
#--------------------- Daytime setup process --------------------
#
#################################################################

if Path(DATA_PATH).expanduser().exists() == False:
    print("[LibTask] '.daytime/' folder doesn't exist, path setup starting")
    mainPath = Path(DATA_PATH).expanduser()
    mainPath.mkdir()
    DATA_PATH = str(mainPath) + "/"
else:
    DATA_PATH = str(Path(DATA_PATH).expanduser()) + "/"
