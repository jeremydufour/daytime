# Daytime

![Daytime logo](icons/daytime_icon-512.png)

This is a small program helping you to track your day and your week activity.

Written in Python using the GTK toolkit.

### Screenshots

###### Daytime main interface
This is where you enter your activity:

![Main UI](media/daytime_main_ui.png)

###### Progress review
See the progress you made during the week:

![Progress UI](media/daytime_progress_ui.png)

### Dependencies
Python 3.8 should be installed on your computer first.
On MacOS don't use Python from Homebrew.

You will need these libraries to execute Daytime properly:
###### Libraries
- gobject-introspection
- gtk+3
- cairo
- adwaita-icon-theme

###### Pip3 modules
- pycairo 1.19.1
- PyGObject 3.36.1
- matplotlib 3.3.0

### Starting the application
To start Daytime from your terminal type: ```python3 Daytime.py```
