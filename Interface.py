# *****************************************************************************
# Daytime   An application to help you track your daily activity
# Copyright (C) 2021  Jeremy DUFOUR
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For more information please see <https://www.dufour.work>.
# *****************************************************************************


## External modules ------------------------------------
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, Gio, GLib
import datetime as DT
import re as regex

## Local modules ---------------------------------------
import LibTask
import LibDiagram
import LibSettings
import LibUtil

# Load icon theme (must have done:~ brew install adwaita-icon-theme)
iconTheme = Gtk.IconTheme().get_default()
#print(iconTheme.list_icons())


## Functions -------------------------------------------
def mainQuit(widget):
    global mainWin

    LibTask.saveData()
    LibSettings.saveSettings()
    #Gtk.main_quit()
#END mainQuit

def checkHourFormat(value):
    parserComplete = regex.compile("[0-9]{1,2}:[0-9]{2}")
    result = parserComplete.match(value)
    if result != None:
        hour = result.string
        if len(result.string) == 4:
            hour = "0" + result.string
        return hour
    else:
        print("[Interface-checkHourFormat()] Hour format error")
        return 1

    return 0
#END checkHourFormat


## Classes ---------------------------------------------
class controlBar(Gtk.EventBox):
    def __init__(self):
        Gtk.EventBox.__init__(self)
        self.box = Gtk.ActionBar()
        self.box.set_events(Gdk.EventMask.ENTER_NOTIFY_MASK)
        self.box.set_events(Gdk.EventMask.LEAVE_NOTIFY_MASK)
        self.add(self.box)

        self.pointed = False
        self.show_all()
    #END __init__

    def pack_start(self, widget):
        self.box.pack_start(widget)
    #END pack_start

    def pack_end(self, widget):
        self.box.pack_end(widget)
    #END pack_start

    def pointed_in(self, widget, event):
        self.pointed = True
    #END pointed_in

    def pointed_out(self, widget, event):
        if event.detail != Gdk.NotifyType.INFERIOR:
            #Prevent 'button pressing' event from resetting pointed
            self.pointed = False
    #END pointed_out
#END controlBar


class completionEntry(Gtk.Entry):
    def __init__(self, placedName):
        Gtk.Entry.__init__(self, placeholder_text=placedName, overwrite_mode=False)
        self.set_hexpand(False)
        self.set_vexpand(False)
        self.set_input_hints(Gtk.InputHints.UPPERCASE_WORDS)
        # This method will allow to resize the entry smaller than it is by default
        #self.set_width_chars(20)
        self.set_max_length(20)
        completion = Gtk.EntryCompletion()
        completion.set_inline_selection(True) #enables selection with up and down arrows
        self.set_completion(completion)

        listStore = Gtk.ListStore(str)  #Create a listStore with a string column
        names = LibSettings.settings["taskParam"].keys()
        for row in names:
            listStore.append([row])

        completion.set_model(listStore)
        completion.set_text_column(0)
        self.show_all()
    #END __init__
#END completionEntry


class hourEntry(Gtk.Entry):
    def __init__(self, placedName):
        global syncTime

        Gtk.Entry.__init__(self, placeholder_text=placedName, overwrite_mode=False)
        self.set_hexpand(False)
        self.set_vexpand(False)
        # This method will allow to resize the entry smaller than it is by default
        self.set_width_chars(5)
        self.set_max_length(5)
        self.syncTime = ""
        self.entered = False
        self.connect("button-press-event", self.entry_pressed)
        self.show_all()
    #END __init__

    def entry_pressed(self, widget, event):
        if self.entered == False:
            #add buffer detection
            self.set_text(self.syncTime)
            self.entered = True
    #END entry_pressed
#END hourEntry


class taskSettings(Gtk.Popover):
    global iconTheme

    def __init__(self, parentMain):
        Gtk.Popover.__init__(self)
        self.set_position(Gtk.PositionType.BOTTOM)

        frameBox = Gtk.Frame(label="Task List", margin=10)
        self.add(frameBox)
        listBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        frameBox.add(listBox)
        scrollBox = Gtk.ScrolledWindow(max_content_width=400)
        scrollBox.set_size_request(400,400)
        listBox.add(scrollBox)

        rowList = Gtk.ListBox(vexpand=True)
        taskParam = LibSettings.settings["taskParam"]

        for i in list(taskParam):
            task = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)
            nameLabel = Gtk.Label(label=i)
            task.pack_start(nameLabel,False,False,20)

            colorVal = Gdk.RGBA(0,0,0,1)
            colorVal.parse(taskParam[i])
            colorButton = Gtk.ColorButton(rgba=colorVal)
            colorButton.connect("color-set", self.color_selected, i, parentMain)
            task.pack_end(colorButton, False, False, 20)

            trashButton = Gtk.Button()
            # Need to add an hint
            iconImage = Gtk.Image(pixbuf=iconTheme.load_icon("folder-templates-symbolic",16,Gtk.IconLookupFlags.USE_BUILTIN))
            trashButton.set_image(iconImage)
            trashButton.connect("clicked",self.delete_parameter, task, i)
            task.pack_end(trashButton, False, False, 10)

            rowList.insert(task, -1)
        scrollBox.add(rowList)
    #END __init__

    def color_selected(self, colorButton, taskName, parentMain):
        colorClass = colorButton.get_rgba()
        color = colorClass.to_string()
        LibSettings.setColor(taskName, color)
        parentMain.refresh_diagram()
        parentMain.__refresh__()
    #END color_selected

    def delete_parameter(self, widget, taskElement, setting):
        taskElement.destroy()
        LibSettings.deleteSetting("taskParam", setting)
        self.show_all()
    #END delete_parameter
#END taskSettings


class progressWin(Gtk.Popover):
    graph = None
    data = None
    scrollBox = None

    showEmpty = False
    showHours = False

    def __init__(self):
        global graph
        global scrollBox
        global data
        global showEmpty
        global showHours

        Gtk.Popover.__init__(self)
        self.set_position(Gtk.PositionType.BOTTOM)
        try:
            showEmpty = LibSettings.settings["displayEmptyTime"]
        except KeyError:
            LibSettings.settings["displayEmptyTime"] = False
            showEmpty = False
        try:
            showHours = LibSettings.settings["hourProgress"]
        except KeyError:
            LibSettings.settings["hourProgress"] = False
            showHours = False

        frameBox = Gtk.Frame(label="Task progress", margin=10)
        self.add(frameBox)
        listBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        frameBox.add(listBox)

        typeBox = Gtk.Box()
        typeBox.set_margin_top(7)
        typeBox.set_margin_bottom(2)
        typeButton = Gtk.Switch(active=showEmpty)
        typeButton.connect("state-set", self.display_empty_time)
        typeBox.pack_start(typeButton, False, False, 10)
        typeBox.pack_start(Gtk.Label(label="Show untracked time"), False, False, 10)
        listBox.add(typeBox)

        hourBox = Gtk.Box()
        hourBox.set_margin_top(2)
        hourBox.set_margin_bottom(5)
        hourButton = Gtk.Switch(active=showHours)
        hourButton.connect("state-set", self.display_hours)
        hourBox.pack_start(hourButton, False, False, 10)
        hourBox.pack_start(Gtk.Label(label="Progress in hours"), False, False, 10)
        listBox.add(hourBox)

        scrollBox = Gtk.ScrolledWindow(max_content_width=550)
        scrollBox.set_size_request(580,360)
        listBox.add(scrollBox)

        data = LibTask.getProgress()
        graph = LibDiagram.progressDiagram(data, showEmpty, showHours)

        scrollBox.add(graph)
    #END __init__

    def display_empty_time(self, widget, state):
        global graph
        global scrollBox
        global data
        global showEmpty
        global showHours

        buttonPressed = widget.get_active()
        if buttonPressed == True:
            graph.__refresh__(data, True, showHours)
            LibSettings.settings["displayEmptyTime"] = True
            showEmpty = True
        else:
            graph.__refresh__(data, False, showHours)
            LibSettings.settings["displayEmptyTime"] = False
            showEmpty = False

        scrollBox.show_all()
    #END display_empty_time

    def display_hours(self, widget, state):
        global graph
        global scrollBox
        global data
        global showEmpty
        global showHours

        buttonPressed = widget.get_active()
        if buttonPressed == True:
            graph.__refresh__(data, showEmpty, True)
            LibSettings.settings["hourProgress"] = True
            showHours = True
        else:
            graph.__refresh__(data, showEmpty, False)
            LibSettings.settings["hourProgress"] = False
            showHours = False

        scrollBox.show_all()
    #END display_hours
#END progressWin


class rightPanel(Gtk.Box):
    nameEntry = None
    startEntry = None
    stopEntry = None
    editBar = None
    moButton = None
    tuButton = None
    weButton = None
    thButton = None
    frButton = None
    saButton = None
    suButton = None

    def __init__(self, parentMain):
        global iconTheme

        global nameEntry
        global startEntry
        global stopEntry
        global editBar
        global moButton
        global tuButton
        global weButton
        global thButton
        global frButton
        global saButton
        global suButton

        # Attributes
        self.pointed = False
        self.taskList = LibTask.activityDay(LibTask.dayNb)

        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        self.set_events(Gdk.EventMask.ENTER_NOTIFY_MASK)
        self.set_events(Gdk.EventMask.LEAVE_NOTIFY_MASK)

        #Initialize task window
        scrollBox = Gtk.ScrolledWindow(max_content_width=200)
        scrollBox.add(self.taskList)

        #Initialize task bar with week layout
        taskBar = Gtk.HeaderBar(title=None, hexpand=True)

        #Day buttons
        moButton = Gtk.ToggleButton(label="Mo")
        moButton.connect("toggled", self.day_clicked, 1, parentMain)
        tuButton = Gtk.ToggleButton(label="Tu")
        tuButton.connect("toggled", self.day_clicked, 2, parentMain)
        weButton = Gtk.ToggleButton(label="We")
        weButton.connect("toggled", self.day_clicked, 3, parentMain)
        thButton = Gtk.ToggleButton(label="Th")
        thButton.connect("toggled", self.day_clicked, 4, parentMain)
        frButton = Gtk.ToggleButton(label="Fr")
        frButton.connect("toggled", self.day_clicked, 5, parentMain)
        saButton = Gtk.ToggleButton(label="Sa")
        saButton.connect("toggled", self.day_clicked, 6, parentMain)
        suButton = Gtk.ToggleButton(label="Su")
        suButton.connect("toggled", self.day_clicked, 7, parentMain)
        taskBar.pack_end(suButton)
        taskBar.pack_end(saButton)
        taskBar.pack_end(frButton)
        taskBar.pack_end(thButton)
        taskBar.pack_end(weButton)
        taskBar.pack_end(tuButton)
        taskBar.pack_end(moButton)

        editBar = controlBar()
        editBar.connect("enter-notify-event", editBar.pointed_in)
        editBar.connect("leave-notify-event", editBar.pointed_out)
        #Name entry
        nameEntry = completionEntry("Task name")
        editBar.pack_start(nameEntry)

        #Start entry
        startEntry = hourEntry("00:00")
        startEntry.syncTime = LibTask.getLastTime()
        editBar.pack_start(startEntry)
        #Stop entry
        stopEntry = hourEntry("23:59")
        editBar.pack_start(stopEntry)

        #Watch button
        watchButton = Gtk.Button()
        iconImage = Gtk.Image(pixbuf=iconTheme.load_icon("media-playback-start-symbolic",16,Gtk.IconLookupFlags.USE_BUILTIN))
        watchButton.set_image(iconImage)
        #watchButton.connect("clicked",self.watch_task)
        #editBar.pack_start(watchButton)

        #Delete button
        deleteButton = Gtk.Button()
        iconImage = Gtk.Image(pixbuf=iconTheme.load_icon("list-remove-symbolic",16,Gtk.IconLookupFlags.USE_BUILTIN))
        deleteButton.set_image(iconImage)
        deleteButton.connect("clicked",self.delete_task)
        editBar.pack_end(deleteButton)

        #Add button
        addButton = Gtk.Button()
        iconImage = Gtk.Image(pixbuf=iconTheme.load_icon("list-add-symbolic",16,Gtk.IconLookupFlags.USE_BUILTIN))
        addButton.set_image(iconImage)
        addButton.connect("clicked",self.add_task)
        editBar.pack_end(addButton)

        #--- Add elements to self
        self.pack_start(taskBar,False,True,0)
        self.pack_start(scrollBox,True,True,0)
        self.pack_end(editBar,False,False,0)
        self.show_all()

        self.set_day_button(LibTask.dayNb)
    #END __init__

    def __refresh__(self):
        global nameEntry
        global startEntry
        global stopEntry
        global editBar

        box = self.get_children()[1]
        viewport = box.get_child()
        self.taskList.destroy()
        self.taskList = LibTask.activityDay(LibTask.dayNb)
        viewport.add(self.taskList)
        self.taskList.show()
        self.taskList.connect("enter-notify-event", self.taskList.pointed_in)
        self.taskList.connect("leave-notify-event", self.taskList.pointed_out)
        self.taskList.connect("row-selected",self.list_selected)

        nameEntry.set_text("")
        if startEntry.entered == False:
            startEntry.set_text("")
        startEntry.entered = False
        startEntry.syncTime = LibTask.getLastTime()
        if stopEntry.entered == False:
            stopEntry.set_text("")
        stopEntry.entered = False
        stopEntry.syncTime = ""
    #END __refresh__

    def refresh_edit_bar(self):
        global editBar
        global nameEntry
        global startEntry
        global stopEntry

        if editBar.pointed == False:
            nameEntry.set_text("")
            if startEntry.entered == False:
                startEntry.set_text("")
            startEntry.entered = False
            startEntry.syncTime = LibTask.getLastTime()
            if stopEntry.entered == False:
                stopEntry.set_text("")
            stopEntry.entered = False
            stopEntry.syncTime = ""
    #END refresh_edit_bar

    def delete_task(self, widget):
        widgetList = self.taskList.get_selected_rows()
        if widgetList != None:
            for i in widgetList:
                LibTask.deleteTask(i.get_index())
                i.destroy()
        #Update diagram
        mainWinParent = self.get_parent().get_parent().get_parent()
        if mainWinParent == None:
            print("[rightPanel::delete_task()] rightPanel is not attached to a window !")
        else:
            mainWinParent.refresh_diagram()
    #END delete_task

    def add_task(self, widget):
        global nameEntry
        global startEntry
        global stopEntry

        name = nameEntry.get_text()
        start = startEntry.get_text()
        stop = stopEntry.get_text()

        #Check for empty fields
        if name!="" and start!="" and stop!="":
            # Check format
            start = checkHourFormat(start)
            stop = checkHourFormat(stop)

            if start==1 or stop==1:
                nameEntry.set_text("")
                startEntry.set_text("")
                stopEntry.set_text("")
                return 1

            # Check if stop time don't exceed the day
            if LibUtil.hourConvert(stop) < LibUtil.hourConvert(start):
                LibTask.addTask(name.title(), start, "23:59")
                LibTask.dayNb += 1
                if LibTask.dayNb > 7:
                    LibTask.nextWeek()
                    LibTask.dayNb = 1
                    # Need to add a check date function
                    LibTask.addTask(name.title(), "00:00", stop)
                    LibTask.dayNb = 7
                    LibTask.prevWeek()
                else:
                    LibTask.addTask(name.title(), "00:00", stop)
                    LibTask.dayNb -= 1
            else:
                LibTask.addTask(name.title(), start, stop)

            nameEntry.set_text("")
            startEntry.set_text("")
            startEntry.entered = False
            stopEntry.set_text("")
            stopEntry.entered = False
            startEntry.syncTime = stop

        else:
            nameEntry.set_text("")
            startEntry.set_text("")
            stopEntry.set_text("")

        self.__refresh__()
        #Update diagram
        mainWinParent = self.get_parent().get_parent().get_parent()
        if mainWinParent == None:
            print("[Interface-rightPanel::add_task()] rightPanel is not attached to a window with diagram !")
        else:
            mainWinParent.refresh_diagram()
    #END add_task

    def list_selected(self, widget, event):
        #automatically display content from selected
        global nameEntry
        global startEntry
        global stopEntry

        selection = self.taskList.get_selected_rows()
        if len(selection) == 1:
            taskElements = selection[0].get_children()[0].get_children()
            nameEntry.set_text(taskElements[0].get_label())
            startEntry.set_text(taskElements[1].get_label())
            stopEntry.set_text(taskElements[2].get_label())
    #END list_selected

    def day_clicked(self, widget, nb, parentMain):
        global moButton
        global tuButton
        global weButton
        global thButton
        global frButton
        global saButton
        global suButton

        buttonPressed = widget.get_active()
        if buttonPressed == True:
            self.taskList.destroy()
            LibTask.dayNb = nb
            self.taskList = LibTask.activityDay(nb)

            if nb == 1:
                #moButton
                tuButton.set_active(False)
                weButton.set_active(False)
                thButton.set_active(False)
                frButton.set_active(False)
                saButton.set_active(False)
                suButton.set_active(False)
            elif nb == 2:
                #tuButton
                moButton.set_active(False)
                weButton.set_active(False)
                thButton.set_active(False)
                frButton.set_active(False)
                saButton.set_active(False)
                suButton.set_active(False)
            elif nb == 3:
                #weButton
                moButton.set_active(False)
                tuButton.set_active(False)
                thButton.set_active(False)
                frButton.set_active(False)
                saButton.set_active(False)
                suButton.set_active(False)
            elif nb == 4:
                #thButton
                moButton.set_active(False)
                tuButton.set_active(False)
                weButton.set_active(False)
                frButton.set_active(False)
                saButton.set_active(False)
                suButton.set_active(False)
            elif nb == 5:
                #frButton
                moButton.set_active(False)
                tuButton.set_active(False)
                weButton.set_active(False)
                thButton.set_active(False)
                saButton.set_active(False)
                suButton.set_active(False)
            elif nb == 6:
                #saButton
                moButton.set_active(False)
                tuButton.set_active(False)
                weButton.set_active(False)
                thButton.set_active(False)
                frButton.set_active(False)
                suButton.set_active(False)
            elif nb == 7:
                #suButton
                moButton.set_active(False)
                tuButton.set_active(False)
                weButton.set_active(False)
                thButton.set_active(False)
                frButton.set_active(False)
                saButton.set_active(False)
            else:
                print("[rightPanel::day_clicked()] wrong day number !")

            parentMain.refresh_date()
            self.__refresh__()
    #END day_clicked

    def set_day_button(self, nb):
        global moButton
        global tuButton
        global weButton
        global thButton
        global frButton
        global saButton
        global suButton

        moButton.set_active(False)
        tuButton.set_active(False)
        weButton.set_active(False)
        thButton.set_active(False)
        frButton.set_active(False)
        saButton.set_active(False)
        suButton.set_active(False)
        if nb == 1:
            moButton.set_active(True)
        elif nb ==2:
            tuButton.set_active(True)
        elif nb == 3:
            weButton.set_active(True)
        elif nb == 4:
            thButton.set_active(True)
        elif nb == 5:
            frButton.set_active(True)
        elif nb == 6:
            saButton.set_active(True)
        elif nb == 7:
            suButton.set_active(True)
        else:
            print("[rightPanel::set_day_button()] wrong day number !")
    #END set_day_button
#END rightPane


class mainWindow(Gtk.ApplicationWindow):
    diagram = None
    rPanel = None
    menuBar = None
    leftBox = None

    def __init__(self, *args, **kwargs):
        # Externals
        global iconTheme
        # Internals
        global diagram
        global rPanel
        global menuBar
        global leftBox

        super().__init__(*args, **kwargs)

        # This will be in the windows group and have the "win" prefix
        max_action = Gio.SimpleAction.new_stateful(
            "maximize", None, GLib.Variant.new_boolean(False)
        )
        max_action.connect("change-state", self.on_maximize_toggle)
        self.add_action(max_action)

        # Keep it in sync with the actual state
        self.connect(
            "notify::is-maximized",
            lambda obj, pspec: max_action.set_state(
                GLib.Variant.new_boolean(obj.props.is_maximized)
            ),
        )

        # Connect destry event to main quit
        self.connect("destroy", mainQuit)

        self.set_default_size(1000,600)
        self.move(400,0)

        localDate = DT.date.today().isocalendar()  # [year, week, day]
        LibTask.loadData(localDate[0],localDate[1],localDate[2])

        LibSettings.loadSettings()

        mainGrid = Gtk.Grid()
        self.add(mainGrid)
        mainPane = Gtk.Paned()
        mainPane.set_wide_handle(True)

        currentDate = DT.date.fromisocalendar(LibTask.yearNb, LibTask.weekNb, LibTask.dayNb)
        menuTitle = LibUtil.dayConvert(LibTask.dayNb) + ", " + LibUtil.monthConvert(currentDate.month) + " " + str(currentDate.day) \
                    + ", " + str(LibTask.yearNb) + " (W " + str(LibTask.weekNb) + ")"

        menuBar = Gtk.HeaderBar(hexpand=True, title=menuTitle)
        menuBar.set_hexpand(True)

        taskButton = Gtk.Button(label="Tasks")
        taskButton.connect("clicked", self.show_tasks)
        menuBar.pack_start(taskButton)

        progressButton = Gtk.Button(label="Progress")
        progressButton.connect("clicked", self.show_progress)
        menuBar.pack_start(progressButton)

        # Arrow buttons
        rightButton = Gtk.Button()
        iconImage = Gtk.Image(pixbuf=iconTheme.load_icon("go-next-symbolic",16,Gtk.IconLookupFlags.USE_BUILTIN))
        rightButton.set_image(iconImage)
        rightButton.connect("clicked", self.next_week_clicked)
        menuBar.pack_end(rightButton)

        leftButton = Gtk.Button()
        iconImage = Gtk.Image(pixbuf=iconTheme.load_icon("go-previous-symbolic",16,Gtk.IconLookupFlags.USE_BUILTIN))
        leftButton.set_image(iconImage)
        leftButton.connect("clicked", self.past_week_clicked)
        menuBar.pack_end(leftButton)

        todayButton = Gtk.Button(label="Today")
        todayButton.connect("clicked", self.today_clicked)
        menuBar.pack_end(todayButton)

        mainGrid.attach(menuBar,0,0,2,1)

        # Right Panel
        rPanel = rightPanel(self)
        rPanel.width_request=100
        rPanel.set_vexpand(True)

        mainPane.pack2(rPanel,False,False)

        # Task diagram
        leftBox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, hexpand=True, vexpand=True)

        if True:
            diagram = LibDiagram.barDiagram(LibTask.taskData["w_"+str(LibTask.weekNb)])
        else:
            diagram = LibDiagram.cairoGtkDiagram(LibTask.taskData["w_"+str(LibTask.weekNb)])
        diagram.show_all()

        leftBox.pack_start(diagram, False, True, 0)
        mainPane.pack1(leftBox,True,True)

        #Adding Paned widget to grid
        mainGrid.attach(mainPane,0,1,1,1)

        self.connect("button-press-event", self.win_click)
    #END __init__

    def on_maximize_toggle(self, action, value):
        action.set_state(value)
        if value.get_boolean():
            self.maximize()
        else:
            self.unmaximize()

    def refresh_date(self):
        global menuBar

        currentDate = DT.date.fromisocalendar(LibTask.yearNb, LibTask.weekNb, LibTask.dayNb)
        menuTitle = LibUtil.dayConvert(LibTask.dayNb) + ", " + LibUtil.monthConvert(currentDate.month) + " " + str(currentDate.day) \
                    + ", " + str(LibTask.yearNb) + " (W " + str(LibTask.weekNb) + ")"

        menuBar.set_title(menuTitle)
        menuBar.show()
    #END refreshDate

    def refresh_diagram(self):
        global leftBox
        global diagram

        diagram.__refresh__(LibTask.taskData["w_"+str(LibTask.weekNb)])
    #END refresh_diagram

    def __refresh__(self):
        global rPanel

        self.refresh_diagram()
        self.refresh_date()
        rPanel.set_day_button(LibTask.dayNb)
        rPanel.__refresh__()
        rPanel.show_all()
    #END __refresh__

    def past_week_clicked(self, widget):
        # Access to the past week and create it if it's missing in the current year
        global rPanel

        LibTask.prevWeek()
        rPanel.taskList.destroy()
        rPanel.taskList = LibTask.activityDay(1)
        self.__refresh__()
    #END past_week_clicked

    def next_week_clicked(self, widget):
        # Access to the next week and create it if it's missing in the current year
        global rPanel

        LibTask.nextWeek()
        rPanel.taskList.destroy()
        rPanel.taskList = LibTask.activityDay(1)
        self.__refresh__()
    #END next_week_clicked

    def show_tasks(self, widget):
        taskPop = taskSettings(self)
        taskPop.set_relative_to(widget)
        taskPop.show_all()
        taskPop.popup()
    #END show_tasks

    def show_progress(self, widget):
        pop = progressWin()
        pop.set_relative_to(widget)
        pop.show_all()
        pop.popup()
    #END show_progress

    def win_click(self, widget, event):
        global rPanel

        if rPanel.taskList.pointed == False:
            rPanel.taskList.unselect_all()
        rPanel.refresh_edit_bar()
    #END win_click

    def today_clicked(self, widget):
        global menuBar
        global rPanel

        localDate = DT.date.today().isocalendar()

        if LibTask.yearNb != localDate[0]:
            LibTask.saveData()
            LibTask.loadData(localDate[0], localDate[1], localDate[2])
        else:
            LibTask.dayNb = localDate[2]
            LibTask.weekNb = localDate[1]
            LibTask.yearNb = localDate[0]

        rPanel.taskList.destroy()
        rPanel.taskList = LibTask.activityDay(LibTask.dayNb)

        self.__refresh__()
    #END today_clicked
#END mainWindow
