# *****************************************************************************
# Daytime   An application to help you track your daily activity
# Copyright (C) 2021  Jeremy DUFOUR
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# For more information please see <https://www.dufour.work>.
# *****************************************************************************

# External modules ---------------------------------------
import sys

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gio, Gtk, GdkPixbuf


# Local modules ------------------------------------------
import Interface


class aboutInfo(Gtk.AboutDialog):
    def __init__(self, p_transient, p_modal):
        logoImage = GdkPixbuf.Pixbuf.new_from_file("icons/daytime_icon-128.png")

        super().__init__(transient_for=p_transient, modal=p_modal,
        authors=["Jeremy DUFOUR"],
        artists=["Jeremy DUFOUR"],
        program_name="Daytime",
        logo=logoImage,
        version="0.1",
        comments="An application to help you track your daily activity",
        license_type=Gtk.License.GPL_3_0_ONLY
        )

        self.connect("response", self.on_close)
    #END __init__

    def on_close(self, widget, response):
        if response == Gtk.ResponseType.DELETE_EVENT or response == Gtk.ResponseType.CLOSE:
            self.close()
    #END on_close


class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            application_id="work.dufour.daytime",
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            **kwargs
            )
        self.window = None

        self.add_main_option(
            "test",
            ord("t"),
            GLib.OptionFlags.NONE,
            GLib.OptionArg.NONE,
            "Command line test",
            None
            )

    def do_startup(self):
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)

        builder = Gtk.Builder.new_from_file("./app-menu.xml")
        self.set_menubar(builder.get_object("app-menu"))

    def do_activate(self):
        if not self.window:
            self.window = Interface.mainWindow(application=self, title="Daytime")
        self.window.present()
        self.window.show_all()

    def do_command_line(self, command_line):
        options = command_line.get_options_dict()
        # convert GVariantDict -> GVariant -> dict
        options = options.end().unpack()

        if "test" in options:
            print("Test argument recieved: %s" % options["test"])

        self.activate()
        return 0

    def on_about(self, action, param):
        about_dialog = aboutInfo(p_transient=self.window, p_modal=True)
        about_dialog.present()

    def on_quit(self, action, param):
        print("quiting")
        Interface.mainQuit(None)
        self.quit()


if __name__ == "__main__":
    app = Application()
    app.run(sys.argv)
